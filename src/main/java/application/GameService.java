package application;

import domain.Brick;
import domain.GameBoard;
import org.apache.commons.lang3.time.StopWatch;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Stack;

public class GameService {

    private static GameBoard gameBoard;
    private Stack<Brick> sourceStack;
    private Stack<Brick> targetStack;
    private Stack<Brick> completeStack = createCompleteStack();
    private int moveCounter = 0;
    private StopWatch stopWatch = new StopWatch();

    public static GameService createGameRunner() {
        return new GameService();
    }

    public void startNewGame() {
        prepareBoard();
        runGame();
    }

    private void moveBrick() {
        if (isMoveAllowed()) {
            targetStack.add(sourceStack.pop());
            this.moveCounter++;
        }
    }

    private boolean isMoveAllowed() {
        return (targetStack.empty() || targetStack.peek().value > sourceStack.peek().value);
    }

    private Stack<Brick> createCompleteStack() {
        Stack<Brick> completeStack = new Stack<>();
        for (Brick brick : Brick.values()) {
            completeStack.push(brick);
        }
        return completeStack;
    }

    private boolean isWinConditionReached() {
        return completeStack.equals(targetStack);
    }

    private void prepareBoard() {
        gameBoard = GameBoard.createNewGameBoard();
    }

    private void runGame() {
        printGameDescription();
        stopWatch.start();
        while (!isWinConditionReached()) {
            printBricksValues();
            printStacksToConsole();
            clearSelection();
            selectSourceStack(getPlayerInput());
            selectTargetStack(getPlayerInput());
            moveBrick();
        }
        stopWatch.stop();
        printScore();
    }

    public void selectSourceStack(int stackNumber) {
        this.sourceStack = gameBoard.selectStackWithNumber(stackNumber);
    }

    public void selectTargetStack(int stackNumber) {
        this.targetStack = gameBoard.selectStackWithNumber(stackNumber);
    }

    private void clearSelection() {
        this.sourceStack = null;
        this.targetStack = null;
    }

    private int getPlayerInput() {
        try {
            Scanner playerInput = new Scanner(System.in);
            System.out.println("choose stack.");
            int stackNumber = playerInput.nextInt();
            while (stackNumber > 3 || stackNumber < 1) {
                System.out.println("wrong stack number.");
                stackNumber = playerInput.nextInt();
            }
            return stackNumber;
        } catch (InputMismatchException e) {
            System.out.println("choose a number 1,2 or 3.");
            return getPlayerInput();
        }
    }

    private void printStacksToConsole() {
        System.out.println("Stos nr. 1: " + gameBoard.getFirstStack());
        System.out.println("Stos nr. 2: " + gameBoard.getSecondStack());
        System.out.println("Stos nr. 3: " + gameBoard.getThirdStack());
    }

    private void printNumberOfMoves() {
        System.out.println("liczba ruchów: " + moveCounter);
    }

    private void printTimeInSeconds() {
        System.out.println("ukończono w: " + stopWatch.getTime() / 1000 + " sekund.");
    }

    private void printGameDescription() {
        System.out.println("Ułóż klocki na jednym stosie zaczynając od największego do najmniejszego.");
        System.out.println("Zasady:");
        System.out.println("1. można przenosić tylko 1 klocek na raz wskazując najpierw numer kolumny źródłowej a następnie docelowej.");
        System.out.println("2. każdy klocek możesz położyć tylko na klocku od niego większym lub na pustym stosie. ");
    }

    private void printBricksValues() {
        System.out.println("wartości klocków:");
        for (Brick brick : Brick.values()) {
            System.out.println(brick + " : " + brick.value);
        }
    }

    private void printScore() {
        printTimeInSeconds();
        printNumberOfMoves();
    }

}
