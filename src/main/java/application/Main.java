package application;

public class Main {

    private static GameService gameRunner = GameService.createGameRunner();

    public static void main(String[] args) {
        gameRunner.startNewGame();
    }
}
