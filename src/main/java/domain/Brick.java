package domain;

public enum Brick {
    RED(5),
    BLUE(4),
    YELLOW(3),
    GREEN(2),
    BLACK(1);

    public final int value;

    private Brick(int value) {
        this.value = value;
    }

}
