package domain;

import java.util.*;

public class GameBoard {
    private Stack<Brick> firstStack;
    private Stack<Brick> secondStack;
    private Stack<Brick> thirdStack;

    private GameBoard() {
        this.firstStack = new Stack<Brick>();
        this.secondStack = new Stack<Brick>();
        this.thirdStack = new Stack<Brick>();
    }

    public static GameBoard createNewGameBoard() {
        GameBoard gameBoard = new GameBoard();
        gameBoard.initializeStacks();
        return gameBoard;
    }

    public Stack<Brick> getFirstStack() {
        return firstStack;
    }

    public Stack<Brick> getSecondStack() {
        return secondStack;
    }

    public Stack<Brick> getThirdStack() {
        return thirdStack;
    }

    public Stack<Brick> selectStackWithNumber(int stackNumber) {
        switch (stackNumber) {
            case 1:
                return this.getFirstStack();
            case 2:
                return this.getSecondStack();
            case 3:
                return this.getThirdStack();
            default:
                return null;
        }
    }

    private void initializeStacks() {
        Random randomNumber = new Random();
        List<Brick> bricks = new ArrayList<>(Arrays.asList(Brick.values()));
        for (int i = bricks.size(); i > 0; i--) {
            int brickNumber = randomNumber.nextInt(i);
            int stackNumber = randomNumber.nextInt(3) + 1;
            if (brickNumber == bricks.size()) {
                brickNumber--;
            }
            selectStackWithNumber(stackNumber).push(bricks.get(brickNumber));
            bricks.remove(brickNumber);
        }
    }
}
